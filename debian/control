Source: orocos-rtt
Priority: optional
Section: libs
Maintainer: Debian Science Maintainers <debian-science-maintainers@lists.alioth.debian.org>
Uploaders: Jochen Sprickerhof <debian@jochen.sprickerhof.de>,
	   Leopold Palomo-Avellaneda <leo@alaxarxa.net>
Build-Depends: debhelper (>= 9), cmake (>=2.6.0), dh-exec (>=0.3),
	libboost-dev, libboost-graph-dev, libboost-thread-dev, libboost-test-dev, libboost-filesystem-dev, 
	libomniorb4-dev, omniidl, gperf-ace, libxml-xpath-perl, pkg-config, libxerces-c-dev,
	libxenomai-dev [ amd64 arm armeb armel i386 powerpc powerpcspe ] 
Standards-Version: 3.9.6
Vcs-Browser: http://anonscm.debian.org/gitweb/?p=debian-science/packages/orocos/rtt.git
Vcs-Git: git://anonscm.debian.org/debian-science/packages/orocos/rtt.git
Homepage: http://www.orocos.org

Package: liborocos-rtt-gnulinux2.8
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Conflicts: liborocos-rtt1
Description: Orocos Real-Time Toolkit library (gnulinux)
 Orocos (http://www.orocos.org) is the acronym of the
 Open Robot Control Software project. The project's aim is to develop
 a general-purpose, free software, and modular framework
 for robotand machine control.
 .
 The Orocos Real-Time Toolkit (RTT) is not an application
 in itself, but it provides the infrastructure and the
 functionalities to build robotics applications in C++. The
 emphasis is on real-time, online interactive and
 component based applications.
 .
 This package provides the RTT library against the gnulinux target.

Package: liborocos-rtt-corba-gnulinux2.8
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}, liborocos-rtt-gnulinux2.8 (=${binary:Version})
Conflicts: liborocos-rtt1
Description: Orocos Real-Time Toolkit for CORBA library (gnulinux)
 The Orocos Real-Time Toolkit (RTT) is not an application
 in itself, but it provides the infrastructure and the
 functionalities to build robotics applications in C++. The
 emphasis is on real-time, online interactive and
 component based applications.
 .
 This package provides the binding to use the corba transport
 with the RTT library (gnulinux target).

Package: liborocos-rtt-gnulinux2.8-dev
Section: libdevel
Architecture: any
Depends: ${misc:Depends}, liborocos-rtt-gnulinux2.8 (= ${binary:Version}),
	liborocos-rtt-common2.8-dev, 
	liborocos-rtt-corba-gnulinux2.8 (= ${binary:Version})
Conflicts: liborocos-rtt-dev, liborocos-rtt-gnulinux-dev
Replaces: liborocos-rtt-gnulinux-dev
Provides: liborocos-rtt-gnulinux-dev
Description: Orocos Real-Time Toolkit development files (gnulinux)
 The Orocos Real-Time Toolkit (RTT) is not an application
 in itself, but it provides the infrastructure and the
 functionalities to build robotics applications in C++. The
 emphasis is on real-time, online interactive and
 component based applications.
 .
 This package provides the stuff needed to develop Orocos applications
 against the gnulinux target.

Package: liborocos-rtt-corba-xenomai2.8
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}, liborocos-rtt-xenomai2.8 (=${binary:Version})
Conflicts: liborocos-rtt1
Description: Orocos Real-Time Toolkit for CORBA library (xenomai)
 The Orocos Real-Time Toolkit (RTT) is not an application
 in itself, but it provides the infrastructure and the
 functionalities to build robotics applications in C++. The
 emphasis is on real-time, online interactive and
 component based applications.
 .
 This package provides the binding to use the corba transport
 with the RTT library (xenomai target).

Package: liborocos-rtt-xenomai2.8-dev
Section: libdevel
Architecture: any
Depends: ${misc:Depends}, liborocos-rtt-xenomai2.8 (= ${binary:Version}), 
	liborocos-rtt-common2.8-dev,
	liborocos-rtt-corba-xenomai2.8 (= ${binary:Version}),
	libxenomai-dev
Conflicts: liborocos-rtt-dev, liborocos-rtt-xenomai-dev
Replaces: liborocos-rtt-xenomai-dev
Provides: liborocos-rtt-xenomai-dev
Description: Orocos Real-Time Toolkit development files (xenomai)
 The Orocos Real-Time Toolkit (RTT) is not an application
 in itself, but it provides the infrastructure and the
 functionalities to build robotics applications in C++. The
 emphasis is on real-time, online interactive and
 component based applications.
 .
 This package provides the stuff needed to develop Orocos applications
 against the xenomai target plus corba transport.

Package: liborocos-rtt-xenomai2.8
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Conflicts: liborocos-rtt1
Description: Orocos Real-Time Toolkit library (xenomai) target
 The Orocos Real-Time Toolkit (RTT) is not an application
 in itself, but it provides the infrastructure and the
 functionalities to build robotics applications in C++. The
 emphasis is on real-time, online interactive and
 component based applications.
 .
 This package provides the RTT library against the xenomai target.


Package: liborocos-rtt-common2.8-dev
Section: libdevel
Architecture: any
Depends: ${misc:Depends}, libboost-dev, libboost-graph-dev, libboost-thread-dev, libboost-filesystem-dev
Conflicts: liborocos-rtt-dev, liborocos-rtt-common-dev
Replaces: liborocos-rtt-common-dev
Provides: liborocos-rtt-common-dev
Description: Orocos Real-Time Toolkit common development files
 The Orocos Real-Time Toolkit (RTT) is not an application
 in itself, but it provides the infrastructure and the
 functionalities to build robotics applications in C++. The
 emphasis is on real-time, online interactive and
 component based applications.
 .
 This package provides common files used by all the targetsof the
 RTT library.
