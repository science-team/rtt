#!/usr/bin/make -f

# Hardening.
export DEB_BUILD_MAINT_OPTIONS=hardening=+all,-pie
DEB_BUILD_OPTIONS += nocheck


CPPFLAGS:=$(shell dpkg-buildflags --get CPPFLAGS)
CFLAGS:=$(shell dpkg-buildflags --get CFLAGS)
CXXFLAGS:=$(shell dpkg-buildflags --get CXXFLAGS)
LDFLAGS:=$(shell dpkg-buildflags --get LDFLAGS) -Wl,--as-needed -Wl,-z,defs

CFLAGS+=$(CPPFLAGS)
CXXFLAGS+=$(CPPFLAGS)

# These are used for cross-compiling and for saving the configure script
# from having to guess our platform (since we know it already)
DEB_HOST_GNU_TYPE:=$(shell dpkg-architecture -qDEB_HOST_GNU_TYPE)
DEB_BUILD_GNU_TYPE:=$(shell dpkg-architecture -qDEB_BUILD_GNU_TYPE)
DEB_HOST_ARCH:=$(shell dpkg-architecture -qDEB_HOST_ARCH)

# Multiarch.
DEB_HOST_MULTIARCH ?= $(shell dpkg-architecture -qDEB_HOST_MULTIARCH)

# Xenomai archs: amd64 arm armeb armel i386 powerpc powerpcspe
ifneq (,$(filter amd64 arm armeb armel i386 powerpc powerpcspe,$(DEB_HOST_ARCH)))
	BUILD_TARGET = gnuxen
else
	BUILD_TARGET = gnulinux
endif


#### Configure ####
override_dh_auto_configure:
	mkdir -p dbuild.gnulinux
	dh_auto_configure --builddirectory=dbuild.gnulinux -- -DCMAKE_BUILD_TYPE=RelWithDebInfo \
	-DENABLE_CORBA=ON \
	-DCORBA_IMPLEMENTATION:STRING=OMNIORB \
	-DENABLE_TESTS=OFF \
	-DOROCOS_TARGET=gnulinux
ifeq ($(BUILD_TARGET),gnuxen)
	mkdir -p dbuild.xenomai
	dh_auto_configure --builddirectory=dbuild.xenomai -- -DCMAKE_BUILD_TYPE=RelWithDebInfo \
	-DENABLE_CORBA=ON \
	-DCORBA_IMPLEMENTATION:STRING=OMNIORB \
	-DENABLE_TESTS=OFF \
	-DOROCOS_TARGET=xenomai
endif	

#### Build ####
	
override_dh_auto_build:
	cd dbuild.gnulinux; $(MAKE) idl;
	dh_auto_build --builddirectory=dbuild.gnulinux	
#	cd dbuild.gnulinux; $(MAKE) idl; $(MAKE) $(MAKE_FLAGS); #$(MAKE) docapi dochtml VERBOSE=1
ifeq ($(BUILD_TARGET),gnuxen)
	cd dbuild.xenomai; $(MAKE) idl;
	dh_auto_build --builddirectory=dbuild.xenomai
#	cd dbuild.xenomai; $(MAKE) idl; $(MAKE) $(MAKE_FLAGS); #$(MAKE) docapi dochtml VERBOSE=1
endif	

override_dh_auto_clean:
	rm -rf dbuild.xenomai
	rm -rf dbuild.gnulinux
	dh_auto_clean

override_dh_auto_test:
	dh_auto_test -O--buildsystem=cmake -O--parallel -O--builddirectory=dbuild.gnulinux
ifeq ($(BUILD_TARGET),gnuxen)
	dh_auto_test -O--buildsystem=cmake -O--parallel -O--builddirectory=dbuild.xenomai
endif  

override_dh_auto_install:
	dh_auto_install -O--buildsystem=cmake -O--parallel -O--builddirectory=dbuild.gnulinux -O--list-missing
ifeq ($(BUILD_TARGET),gnuxen)
	dh_auto_install -O--buildsystem=cmake -O--parallel -O--builddirectory=dbuild.xenomai -O--list-missing
endif  

# for the -dbg packages
#override_dh_strip:
#	dh_strip -O--builddirectory=dbuild.gnulinux -a --dbg-package=liborocos-rtt-gnulinux2.8-dbg
#ifeq ($(BUILD_TARGET),gnuxen)
#	dh_strip -O--builddirectory=dbuild.xenomai -a --dbg-package=liborocos-rtt-xenomai2.8-dbg
#endif  

get-orig-source:
	uscan --verbose --force-download --repack --compress xz

%:	
	dh $@ --buildsystem=cmake --parallel 

